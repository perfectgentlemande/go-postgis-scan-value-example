package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

type PGConfig struct {
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Address  string `yaml:"addr"`
	DBName   string `yaml:"dbname"`
}
func (pg PGConfig) String() string {
	return fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable", pg.User, pg.Password, pg.Address, pg.DBName)
}

type Location struct {
	ID              string   `db:"id"`
	Geopoint        Geopoint `db:"geopoint"`
	Address         string   `db:"address"`
	Country         string   `db:"country"`
	Locality        string   `db:"locality"`
	PorchNumber     *string  `db:"porch_number"`
	PremiseNumber   string   `db:"premise_number"`
	ThoroughFare    string   `db:"thorough_fare"`
	PositionInRoute int      `db:"position_in_route"`
	OrderID         int      `db:"order_id"`
	EmployeeID      int      `db:"employee_id"`
}
func (loc Location) String() string {
	return fmt.Sprintf("Geopoint long: %f lat: %f", loc.Geopoint.Lon, loc.Geopoint.Lat)
}

type config struct {
	PostgreSQL PGConfig `yaml:"postgres"`
}

func parseConfig(path string) (*config, error) {

	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	c := config{}
	err = yaml.NewDecoder(f).Decode(&c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}

const queryGetLocation = `
SELECT 
	'0' as id, 
	ST_SetSRID(ST_MakePoint(37.658904, 55.770350), 4326) as geopoint, 
	'Россия, Москва, Новая Басманная улица, 17' as address, 
	'Россия' as country, 
	'Москва' as locality, 
	NULL as porch_number, 
	'17' as premise_number,
	'Новая Басманная улица' as thorough_fare, 
	0 as position_in_route, 
	1 as order_id, 
	1 as employee_id;`

const queryInsertLocation = `
INSERT INTO location 
(id, geopoint, address, country, locality, porch_number, premise_number, thorough_fare, position_in_route, order_id, employee_id)
VALUES 
($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
RETURNING id;`

const queryGetLocationWithID = `
SELECT id, geopoint, address, country, locality, porch_number, premise_number, thorough_fare, position_in_route, order_id, employee_id
FROM location
WHERE id = $1;`

func main() {

	configPath := flag.String("c", "config.yaml", "set config path")
	flag.Parse()
	cfg, err := parseConfig(*configPath)
	if err != nil {
		log.Fatal(errors.Wrap(err, "cannot parse config"))
	}

	db, err := sqlx.Connect("postgres", cfg.PostgreSQL.String())
	if err != nil {
		log.Fatal(errors.Wrap(err, "cannot connect"))
	}
	err = db.Ping()
	if err != nil {
		log.Fatal(errors.Wrap(err, "cannot ping"))
	}

	// StructScan with SELECT
	rows, err := db.Queryx(queryGetLocation)
	if err != nil {
		log.Fatal(errors.Wrap(err, "query error"))
	}
	for rows.Next() {
		loc := Location{}
		err := rows.StructScan(&loc)
		if err != nil {
			log.Fatal(errors.Wrap(err, "scan error"))
		}

		fmt.Println(loc)
	}

	locToCheck := Location{
		ID: "some-uuid-id",
		Geopoint: Geopoint{
			Lon: 37.658904,
			Lat: 55.770350,
		},
		Address: "Россия, Москва, Новая Басманная улица, 17",
		Country: "Россия",
		Locality: "Москва",
		PorchNumber: nil,
		PremiseNumber: "17",
		ThoroughFare: "Новая Басманная улица",
		PositionInRoute: 0,
		OrderID: 1,
		EmployeeID: 111111,
	}

	var newID string
	err = db.Get(
		&newID,
		queryInsertLocation,
		locToCheck.ID,
		locToCheck.Geopoint,
		locToCheck.Address,
		locToCheck.Country,
		locToCheck.Locality,
		locToCheck.PorchNumber,
		locToCheck.PremiseNumber,
		locToCheck.ThoroughFare,
		locToCheck.PositionInRoute,
		locToCheck.OrderID,
		locToCheck.EmployeeID)
	if err != nil {
		log.Fatal(errors.Wrap(err, "query or scan error"))
	}

	// StructScan with SELECT
	rows, err = db.Queryx(queryGetLocationWithID, newID)
	if err != nil {
		log.Fatal(errors.Wrap(err, "query error"))
	}
	for rows.Next() {
		loc := Location{}
		err := rows.StructScan(&loc)
		if err != nil {
			log.Fatal(errors.Wrap(err, "scan error"))
		}

		fmt.Println(loc)
	}
}
