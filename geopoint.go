package main

import (
	"database/sql/driver"
	"encoding/binary"
	"github.com/pkg/errors"
	"github.com/twpayne/go-geom"
	"github.com/twpayne/go-geom/encoding/ewkbhex"
)

type Geopoint struct {
	Lon float64
	Lat float64
}

func (gp Geopoint) Value() (driver.Value, error) {
	point := geom.NewPointFlat(geom.XY, []float64{gp.Lon, gp.Lat}).SetSRID(4326)
	return ewkbhex.Encode(point, binary.LittleEndian)
}

func (gp *Geopoint) Scan(value interface{}) error {
	v, ok := value.([]byte)
	if !ok {
		//if the value is NULL there should be v == nil and ok == false
		return nil
	}

	val := string(v)
	if val == "" {
		return errors.New("wrong value")
	}
	p, err := ewkbhex.Decode(val)
	if err != nil {
		return err
	}
	point := Geopoint{
		Lon: p.FlatCoords()[0],
		Lat: p.FlatCoords()[1],
	}
	*gp = point
	return nil
}


// In case if you want to get the multipoint data via PostGIS
type Multipoint []Geopoint

func (mp Multipoint) Value() (driver.Value, error) {
	mpoint, err := mp.Make()
	mpoint.SetSRID(4326)
	if err != nil {
		return nil, err
	}
	return ewkbhex.Encode(mpoint, binary.LittleEndian)
}
func (mp Multipoint) Make() (*geom.MultiPoint, error) {
	mpoint := geom.NewMultiPoint(geom.XY)
	for _, point := range mp {
		p := geom.NewPointFlat(geom.XY, []float64{point.Lon, point.Lat})
		err := mpoint.Push(p)
		if err != nil {
			return nil, err
		}
	}
	return mpoint, nil
}

func (mp *Multipoint) Scan(value interface{}) error {
	v, ok := value.([]byte)
	if !ok {
		//if the value is NULL there should be v == nil and ok == false
		return nil
	}

	val := string(v)
	if val == "" {
		return errors.New("wrong value")
	}
	p, err := ewkbhex.Decode(val)
	if err != nil {
		return err
	}
	points := make([]Geopoint, 0, 0)

	i := 0
	flatCoords := p.FlatCoords()
	for i < len(flatCoords)-1 && i+2 <= len(flatCoords) {
		points = append(points, Geopoint{
			Lon: flatCoords[i],
			Lat: flatCoords[i+1],
		})
		i += 2
	}
	*mp = points
	return nil
}
